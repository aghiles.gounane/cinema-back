package org.sid.cinema.service;

import jakarta.transaction.Transactional;
import org.sid.cinema.entities.*;
import org.sid.cinema.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

@Service
@Transactional
public class CinemaInitServiceImpl implements ICinemaInitService{
    @Autowired
    private VilleRepository villeRepository;
    @Autowired
    private CinemaRepository cinemaRepository;
    @Autowired
    private SalleRepository salleRepository;
    @Autowired
    private PlaceRepository placeRepository;
    @Autowired
    private SeanceRepository seanceRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private FilmRepository filmRepository;
    @Autowired
    private ProjectionRepository projectionRepository;
    @Autowired
    private TicketRepository ticketRepository;


    @Override
    public void initVilles() {
        Stream.of("Paris", "Marsaille","Lyon","Nice").forEach(v->{
            Ville ville = new Ville();
            ville.setName(v);
            villeRepository.save(ville);
        });
    }

    @Override
    public void initCinema() {
        villeRepository.findAll().forEach(v->{
            Stream.of("m2K","IMAX","Allo Ciné","CGR").forEach(c->{
                Cinema cinema = new Cinema();
                cinema.setName(c);
                cinema.setNombreSalles(3+(int)(Math.random()*7));
                cinema.setVille(v);
                cinemaRepository.save(cinema);
            });
                });
    }

    @Override
    public void initSalles() {
        cinemaRepository.findAll().forEach(c->{
        for(int i=0;i<c.getNombreSalles(); i++){
            Salle salle = new Salle();
            salle.setName("Salle "+(i+1));
            salle.setNombrePlaces(20+(int)(Math.random()*35));
            salle.setCinema(c);
            salleRepository.save(salle);
        }
        });
    }

    @Override
    public void initPlaces() {
        salleRepository.findAll().forEach(s->{
            for(int i=0; i< s.getNombrePlaces();i++){
                Place place = new Place();
                place.setNumero(i+1);
                place.setSalle(s);
                placeRepository.save(place);
            }
        });
    }

    @Override
    public void initSeances() {
        DateFormat dateFormat= new SimpleDateFormat("HH:mm");
        Stream.of("12:00","15:00","17:00","19:00","21:00").forEach(s->{
            Seance seance = new Seance();
            try {
                seance.setHeureDebut(dateFormat.parse(s));
                seanceRepository.save(seance);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

        });
    }

    @Override
    public void initCategories() {

        Stream.of("Histoire", "Action", "Fiction","Drama").forEach(c->{
            Categorie categorie = new Categorie();
            categorie.setName(c);
            categoryRepository.save(categorie);
        });
    }

    @Override
    public void initFilms() {
        double[] durees = new double[]{1,1.5,2,2.5,3};
        List<Categorie> categories = categoryRepository.findAll();
        Stream.of("Game Of Thrones", "Seigneur des anneaux", "Spederman","Iron Man","Cat Women","12 Hommes en Colaires","Forest Gump","Green Book","La Ligne Verte","Le Parin", "Le Seigneur des anneaux").forEach(f->{
            Film film = new Film();
            film.setTitre(f);
            film.setDuree(durees[new Random().nextInt(durees.length)]);
            film.setPhoto(f.replaceAll(" ","")+".jpg");
            film.setCategorie(categories.get(new Random().nextInt(categories.size())));
            filmRepository.save(film);
        });
    }

    @Override
    public void initProjections() {
        double[] price = new double[]{30,50,60,70,100};
        villeRepository.findAll().forEach(ville->{
            ville.getCinemas().forEach(cinema->{
                cinema.getSalles().forEach(salle->{
                    filmRepository.findAll().forEach(film->{
                        seanceRepository.findAll().forEach(seance->{
                            Projection projection = new Projection();
                            projection.setDateProjection(new Date());
                            projection.setFilm(film);
                            projection.setPrix(price[new Random().nextInt(price.length)]);
                            projection.setSalle(salle);
                            projection.setSeance(seance);
                            projectionRepository.save(projection);
                        });
                    });
                });
            });
        });

    }

    @Override
    public void initTickets() {
        projectionRepository.findAll().forEach(p->{
            p.getSalle().getPlaces().forEach(place->{
                Ticket ticket = new Ticket();
                ticket.setPlace(place);
                ticket.setPrix(p.getPrix());
                ticket.setProjection(p);
                ticket.setReserve(false);
                ticketRepository.save(ticket);
            });
        });
    }
}
