package org.sid.cinema.repository;

import org.sid.cinema.entities.Projection;
import org.sid.cinema.entities.Salle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectionRepository extends JpaRepository<Projection, Long> {
}
