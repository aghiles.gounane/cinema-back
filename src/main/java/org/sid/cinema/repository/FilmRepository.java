package org.sid.cinema.repository;

import org.sid.cinema.entities.Film;
import org.sid.cinema.entities.Salle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmRepository extends JpaRepository<Film, Long> {
}
