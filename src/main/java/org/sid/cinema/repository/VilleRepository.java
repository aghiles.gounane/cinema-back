package org.sid.cinema.repository;

import org.sid.cinema.entities.Salle;
import org.sid.cinema.entities.Ville;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VilleRepository extends JpaRepository<Ville, Long> {
}
