package org.sid.cinema.repository;

import org.sid.cinema.entities.Salle;
import org.sid.cinema.entities.Seance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeanceRepository extends JpaRepository<Seance, Long> {
}
