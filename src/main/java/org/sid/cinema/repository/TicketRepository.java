package org.sid.cinema.repository;

import org.sid.cinema.entities.Salle;
import org.sid.cinema.entities.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepository extends JpaRepository<Ticket, Long> {
}
