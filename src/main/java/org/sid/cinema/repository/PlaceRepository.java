package org.sid.cinema.repository;

import org.sid.cinema.entities.Place;
import org.sid.cinema.entities.Salle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepository extends JpaRepository<Place, Long> {
}
